﻿namespace proiect_uP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.insertButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.forwardingCheckBox = new System.Windows.Forms.CheckBox();
            this.namesComboBox = new System.Windows.Forms.ComboBox();
            this.f1ComboBox = new System.Windows.Forms.ComboBox();
            this.f2ComboBox = new System.Windows.Forms.ComboBox();
            this.f3ComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.addComboBox = new System.Windows.Forms.ComboBox();
            this.multiplyComboBox = new System.Windows.Forms.ComboBox();
            this.divideComboBox = new System.Windows.Forms.ComboBox();
            this.intDivideComboBox = new System.Windows.Forms.ComboBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.allButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // insertButton
            // 
            this.insertButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.insertButton.Location = new System.Drawing.Point(811, 34);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(133, 23);
            this.insertButton.TabIndex = 0;
            this.insertButton.Text = "Insert Instructions";
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.insertButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.removeButton.Location = new System.Drawing.Point(811, 90);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(133, 23);
            this.removeButton.TabIndex = 1;
            this.removeButton.Text = "Remove Instruction";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // resetButton
            // 
            this.resetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.resetButton.Location = new System.Drawing.Point(811, 146);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(133, 23);
            this.resetButton.TabIndex = 2;
            this.resetButton.Text = "Reset Aplication";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(553, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 133);
            this.label4.TabIndex = 3;
            this.label4.Tag = "";
            this.label4.Text = "label4";
            // 
            // forwardingCheckBox
            // 
            this.forwardingCheckBox.AutoSize = true;
            this.forwardingCheckBox.Location = new System.Drawing.Point(295, 96);
            this.forwardingCheckBox.Name = "forwardingCheckBox";
            this.forwardingCheckBox.Size = new System.Drawing.Size(104, 17);
            this.forwardingCheckBox.TabIndex = 4;
            this.forwardingCheckBox.Text = "Data Forwarding";
            this.forwardingCheckBox.UseVisualStyleBackColor = true;
            // 
            // namesComboBox
            // 
            this.namesComboBox.FormattingEnabled = true;
            this.namesComboBox.Location = new System.Drawing.Point(211, 34);
            this.namesComboBox.Name = "namesComboBox";
            this.namesComboBox.Size = new System.Drawing.Size(102, 21);
            this.namesComboBox.TabIndex = 6;
            this.namesComboBox.SelectedIndexChanged += new System.EventHandler(this.namesComboBox_SelectedValueChanged);
            // 
            // f1ComboBox
            // 
            this.f1ComboBox.FormattingEnabled = true;
            this.f1ComboBox.Location = new System.Drawing.Point(325, 34);
            this.f1ComboBox.Name = "f1ComboBox";
            this.f1ComboBox.Size = new System.Drawing.Size(65, 21);
            this.f1ComboBox.TabIndex = 7;
            // 
            // f2ComboBox
            // 
            this.f2ComboBox.FormattingEnabled = true;
            this.f2ComboBox.Location = new System.Drawing.Point(396, 34);
            this.f2ComboBox.Name = "f2ComboBox";
            this.f2ComboBox.Size = new System.Drawing.Size(65, 21);
            this.f2ComboBox.TabIndex = 8;
            // 
            // f3ComboBox
            // 
            this.f3ComboBox.FormattingEnabled = true;
            this.f3ComboBox.Location = new System.Drawing.Point(470, 34);
            this.f3ComboBox.Name = "f3ComboBox";
            this.f3ComboBox.Size = new System.Drawing.Size(65, 21);
            this.f3ComboBox.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "FP_Add/Sub";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "FP_Multiply";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "FP_Divide";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "INT_Divide";
            // 
            // addComboBox
            // 
            this.addComboBox.FormattingEnabled = true;
            this.addComboBox.Location = new System.Drawing.Point(127, 34);
            this.addComboBox.Name = "addComboBox";
            this.addComboBox.Size = new System.Drawing.Size(53, 21);
            this.addComboBox.TabIndex = 4;
            // 
            // multiplyComboBox
            // 
            this.multiplyComboBox.FormattingEnabled = true;
            this.multiplyComboBox.Location = new System.Drawing.Point(127, 72);
            this.multiplyComboBox.Name = "multiplyComboBox";
            this.multiplyComboBox.Size = new System.Drawing.Size(53, 21);
            this.multiplyComboBox.TabIndex = 5;
            // 
            // divideComboBox
            // 
            this.divideComboBox.FormattingEnabled = true;
            this.divideComboBox.Location = new System.Drawing.Point(127, 110);
            this.divideComboBox.Name = "divideComboBox";
            this.divideComboBox.Size = new System.Drawing.Size(53, 21);
            this.divideComboBox.TabIndex = 6;
            // 
            // intDivideComboBox
            // 
            this.intDivideComboBox.FormattingEnabled = true;
            this.intDivideComboBox.Location = new System.Drawing.Point(127, 148);
            this.intDivideComboBox.Name = "intDivideComboBox";
            this.intDivideComboBox.Size = new System.Drawing.Size(53, 21);
            this.intDivideComboBox.TabIndex = 7;
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 187);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(932, 289);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Instructions";
            this.columnHeader1.Width = 200;
            // 
            // allButton
            // 
            this.allButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.allButton.Location = new System.Drawing.Point(15, 482);
            this.allButton.Name = "allButton";
            this.allButton.Size = new System.Drawing.Size(75, 23);
            this.allButton.TabIndex = 11;
            this.allButton.Text = "Execute all";
            this.allButton.UseVisualStyleBackColor = true;
            this.allButton.Click += new System.EventHandler(this.allButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 517);
            this.Controls.Add(this.allButton);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.intDivideComboBox);
            this.Controls.Add(this.divideComboBox);
            this.Controls.Add(this.f3ComboBox);
            this.Controls.Add(this.multiplyComboBox);
            this.Controls.Add(this.f2ComboBox);
            this.Controls.Add(this.addComboBox);
            this.Controls.Add(this.f1ComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.namesComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.forwardingCheckBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.insertButton);
            this.DoubleBuffered = true;
            this.MinimumSize = new System.Drawing.Size(700, 383);
            this.Name = "Form1";
            this.Tag = "";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button insertButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox forwardingCheckBox;
        private System.Windows.Forms.ComboBox namesComboBox;
        private System.Windows.Forms.ComboBox f1ComboBox;
        private System.Windows.Forms.ComboBox f2ComboBox;
        private System.Windows.Forms.ComboBox f3ComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox addComboBox;
        private System.Windows.Forms.ComboBox multiplyComboBox;
        private System.Windows.Forms.ComboBox divideComboBox;
        private System.Windows.Forms.ComboBox intDivideComboBox;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button allButton;
    }
}

