﻿using System;
using System.Collections.Generic;

namespace proiect_uP
{
    internal class SimulatorBackend
    {
        private int [,] pipelineMatrix;
        private bool m_dataForwarding;
        private int i_instrCount;
        private int i_pipelineInsrtCount;
        private List<string> m_instructionList;
        private string registerPattern = "\\(\\w\\d.*\\w\\d.*.\\w\\d\\)";

        public SimulatorBackend(List<string> instructionList, bool dataForwarding)
        {
            this.m_instructionList = instructionList;
            this.m_dataForwarding = dataForwarding;

            i_instrCount = instructionList.Count;
            i_pipelineInsrtCount = instructionList.Count * 5;

            pipelineMatrix = new int[i_instrCount, i_pipelineInsrtCount + 1];

        }

        public int[,] generateInstructionSteps()
        {
            //for data forwarding being activated everything goes without stalls
            if (m_dataForwarding == true)
            {
                for (int i = 0; i < i_instrCount; i++)
                {
                    for (int j = 1; j <= 5; j++)
                    {
                        pipelineMatrix[i, i+j] = j;
                    }
                }
            }
            else
            {
                for (int i = 0; i < i_instrCount; i++)
                {
                    int currentStage = 1; //the current stage going from 1(IF) to 5(WB) (the value 6 is also reserved for stalls)

                    //this for cound be further optimized as the maximum capacity(adding 5 new step blocks/instruction is never reached)
                    for (int j = 0; j < i_instrCount * 5 + 1; j++)
                    {
                        if (currentStage > 5)
                        {
                            break;
                        }
                        //for 2+ instructions as the first one always goes without stalls
                        if (i != 0)
                        {
                            //regex matches too check registers for stalls
                            //this could be further improved by making a instruction class for ease of use and to beautify the code
                            string match = System.Text.RegularExpressions.Regex.Match(m_instructionList[i], registerPattern).Value;
                            string match2 = System.Text.RegularExpressions.Regex.Match(m_instructionList[i - 1], registerPattern).Value;
                            if (match.Equals(match2))
                            {
                                //checks if the above instruction is a stall/empty space so it doesn't start in the wrong place
                                if (currentStage == 1 && (pipelineMatrix[i - 1, i + j + 1] == 6 || pipelineMatrix[i - 1, i + j + 1] == 0))
                                {
                                    pipelineMatrix[i, i + j + 1] = 0;
                                    j++; //this is needed because of the empty spaced added by stalls, without it the instruction would start too soon
                                }
                                //if the above instruction has at position i + j + 1 a MEM/WB/STALL, the current instruction stalls as well
                                else if ((pipelineMatrix[i - 1, i + j + 1] == 4 || 
                                          pipelineMatrix[i - 1, i + j + 1] == 5 || 
                                          pipelineMatrix[i - 1, i + j + 1] == 6))
                                {
                                    pipelineMatrix[i, i + j + 1] = 6;
                                }
                                //if everything is normal and nothing causes a stall this is the normal path to take
                                else
                                {
                                    pipelineMatrix[i, i + j + 1] = currentStage;
                                    currentStage++;
                                }
                              
                            }
                            //the path if the register regex does not match
                            else
                            {
                                if (pipelineMatrix[i - 1, i + j] == 6 && currentStage == 1)
                                {
                                    pipelineMatrix[i, i + j + 1] = 0;
                                }
                                else if (pipelineMatrix[i - 1, i + j] == 6)
                                {
                                    pipelineMatrix[i, i + j + 1] = 6;
                                }
                                else if (pipelineMatrix[i - 1, i + j] == 0 || pipelineMatrix[i - 1, i + j] == pipelineMatrix[i, i + j])
                                {
                                    pipelineMatrix[i, i + j] = 0;
                                }
                                else
                                {
                                    pipelineMatrix[i, i + j + 1] = currentStage;
                                    currentStage++;
                                }
                            }
                        }
                        //this does the first instruction
                        else
                        {
                            pipelineMatrix[i, i + j + 1] = currentStage;
                            currentStage++;
                        }
                    }

                }
            }
            return pipelineMatrix;
        }
        }
}