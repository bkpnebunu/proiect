﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proiect_uP
{
    public partial class Form1 : Form
    {
        private int columnCount = 1; //column count needed to create and erase listView columns
        private List<string> instructionList = new List<string>();

        // form components initialization
        public Form1()
        {
            InitializeComponent();
            populateCommandDropDown();
            populateExecutionCyclesComboBoxes();
        }

        //populates the name Combo Box using a list manually created
        private void populateCommandDropDown()
        {
            List<String> names = new List<string> { "FP_Add", "FP_Subtract", "FP_Multiply", "FP_Divide", "FP_Load", "FP_Store",
                                                    "INT_Add", "INT_Subtract", "INT_Multiply", "INT_Divide", "INT_Load", "INT_Store",
                                                    "BR_Taken", "BR_Untaken" };
            foreach (String name in names)
            {
                namesComboBox.Items.Add(name);
            }
            namesComboBox.SelectedIndex = 0;
            namesComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void populateFloatRegisters(ComboBox currentModifiedComboBox)
        {
            List<String> values = new List<string> { "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8",
                                                     "F9", "F10", "F11", "F12", "F13", "F14", "F15", "F16" };
            foreach (String value in values)
            {
                currentModifiedComboBox.Items.Add(value);
            }
            currentModifiedComboBox.SelectedIndex = 0;
            currentModifiedComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void populateIntRegisters(ComboBox currentModifiedComboBox)
        {
            List<String> values = new List<string> { "R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8",
                                                     "R9", "R10", "R11", "R12", "R13", "R14", "R15", "R16",
                                                     "R17", "R18", "R19", "R20", "R21", "R22", "R23", "R24",
                                                     "R25", "R26", "R27", "R28", "R29", "R30", "R31", "R32" };
            foreach (String value in values)
            {
                currentModifiedComboBox.Items.Add(value);
            }         
            currentModifiedComboBox.SelectedIndex = 0;
            currentModifiedComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void populateOffsetRegister(ComboBox currentModifiedComboBox)
        {
            currentModifiedComboBox.Items.Add("Offset");
            currentModifiedComboBox.SelectedIndex = 0;
            currentModifiedComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        //populates the execution cycle Combo Boxes
        private void populateExecutionCyclesComboBoxes()
        {
            for (int i = 1; i <= 20; i++)
            {
                addComboBox.Items.Add(i);
                multiplyComboBox.Items.Add(i);
                divideComboBox.Items.Add(i);
                intDivideComboBox.Items.Add(i);
            }

            addComboBox.SelectedIndex = 0;
            multiplyComboBox.SelectedIndex = 0;
            divideComboBox.SelectedIndex = 0;
            intDivideComboBox.SelectedIndex = 0;

            addComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            multiplyComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            divideComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            intDivideComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void clearRegisterComboBox()
        {
            f1ComboBox.Items.Clear();
            f2ComboBox.Items.Clear();
            f3ComboBox.Items.Clear();
        }

        //changes the registers to match the selected instruction
        private void namesComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            clearRegisterComboBox();

            if (namesComboBox.SelectedItem.Equals("FP_Add") ||
                namesComboBox.SelectedItem.Equals("FP_Subtract") ||
                namesComboBox.SelectedItem.Equals("FP_Multiply") ||
                namesComboBox.SelectedItem.Equals("FP_Divide")
                )
            {
                populateFloatRegisters(f1ComboBox);
                populateFloatRegisters(f2ComboBox);
                populateFloatRegisters(f3ComboBox);
            }

            if (namesComboBox.SelectedItem.Equals("INT_Add") ||
                namesComboBox.SelectedItem.Equals("INT_Subtract") ||
                namesComboBox.SelectedItem.Equals("INT_Multiply") ||
                namesComboBox.SelectedItem.Equals("INT_Divide")
                )
            {
                populateIntRegisters(f1ComboBox);
                populateIntRegisters(f2ComboBox);
                populateIntRegisters(f3ComboBox);
            }

            if (namesComboBox.SelectedItem.Equals("FP_Store") ||
                namesComboBox.SelectedItem.Equals("FP_Load")
                )
            {
                populateFloatRegisters(f1ComboBox);
                populateOffsetRegister(f2ComboBox);
                populateIntRegisters(f3ComboBox);
            }

            if (namesComboBox.SelectedItem.Equals("INT_Store") ||
                namesComboBox.SelectedItem.Equals("INT_Load")
                )
            {
                populateIntRegisters(f1ComboBox);
                populateOffsetRegister(f2ComboBox);
                populateIntRegisters(f3ComboBox);
            }

            if (namesComboBox.SelectedItem.Equals("BR_Taken") ||
                namesComboBox.SelectedItem.Equals("BR_Untaken")
                )
            {
                populateOffsetRegister(f2ComboBox);
                populateIntRegisters(f3ComboBox);
            }

        }

        //creates a label with the selected instruction name and selected registers
        private Label createInstructionLabel()
        {
            Label selectedInstruction = null;
            //some error handling to make a differentiation between 3 param instructions and 2 param ones
            //there should be a better way to redo this
            try
            {
                selectedInstruction = new Label
                {
                    Text = namesComboBox.SelectedItem.ToString() + " (" +
                           f1ComboBox.SelectedItem.ToString() + ", " +
                           f2ComboBox.SelectedItem.ToString() + ", " +
                           f3ComboBox.SelectedItem.ToString() + ")"
                };
            }
            catch (NullReferenceException)
            {
                selectedInstruction = new Label
                {
                    Text = namesComboBox.SelectedItem.ToString() + " (" +
                           f2ComboBox.SelectedItem.ToString() + ", " +
                           f3ComboBox.SelectedItem.ToString() + ")"
                };
            }
            selectedInstruction.AutoSize = true;
            return selectedInstruction;
        }

        //adds a new instruction into the listView
        private void insertButton_Click(object sender, EventArgs e)
        {
            Label currentInstruction = createInstructionLabel();
            instructionList.Add(currentInstruction.Text);
            listView1.Items.Add(currentInstruction.Text);

            for (int i = columnCount; i < columnCount + 5; i++)
            {
                listView1.Columns.Add(i.ToString(), 50);
            }
            columnCount += 5;
        }

        private void allButton_Click(object sender, EventArgs e)
        {
            SimulatorBackend simulatorBackend = new SimulatorBackend(instructionList, forwardingCheckBox.Checked);
            string[,] matrix = interpretMatrix(simulatorBackend.generateInstructionSteps()); //turns the int matrix into a string one

            for (int i = 0; i < listView1.Items.Count; i++)
            {
                for (int j = 1; j < matrix.Length/ listView1.Items.Count; j++)
                {
                    listView1.Items[i].SubItems.Add(""); //adds empty listView subitems
                    listView1.Items[i].SubItems[j].Text = matrix[i, j].ToString(); //populates them
                }
            }
        }

        //the matrix comes as an integer matrix for ease of use, and this method does the conversion
        private string[,] interpretMatrix(int [,] intMatrix)
        {
            string[,] stringMatrix = new string[intMatrix.GetLength(0), intMatrix.GetLength(1)];
            for (int i = 0; i < intMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < intMatrix.GetLength(1); j++)
                {
                    switch (intMatrix[i,j])
                    {
                        case 1:
                            stringMatrix[i, j] = "IF";
                            break;
                        case 2:
                            stringMatrix[i, j] = "ID";
                            break;
                        case 3:
                            stringMatrix[i, j] = "EX";
                            break;
                        case 4:
                            stringMatrix[i, j] = "MEM";
                            break;
                        case 5:
                            stringMatrix[i, j] = "WB";
                            break;
                        case 6:
                            stringMatrix[i, j] = "S";
                            break;
                        default:
                            stringMatrix[i, j] = "";
                            break;
                    }
                }
            }
            return stringMatrix;
        }

        //resets the listView
        private void resetButton_Click(object sender, EventArgs e)
        {
            while (instructionList.Count != 0)
            {
                listView1.Items[instructionList.Count - 1].Remove();
                instructionList.RemoveAt(instructionList.Count - 1);
            }
            for (int i = listView1.Columns.Count - 1; i > 0; i--)
            {
                listView1.Columns.RemoveAt(i);
            }
            columnCount = 1;
        }

        //removes the last instruction
        private void removeButton_Click(object sender, EventArgs e)
        {
            if (instructionList.Count != 0) //without it it will crash
            {
                listView1.Items[instructionList.Count - 1].Remove();
                instructionList.RemoveAt(instructionList.Count - 1);
                for (int i = 0; i < 5; i++)
                {
                    listView1.Columns.RemoveAt(listView1.Columns.Count - 1);
                }
                columnCount -= 5;
            }
        }
    }
}
